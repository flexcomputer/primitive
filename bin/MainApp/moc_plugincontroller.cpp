/****************************************************************************
** Meta object code from reading C++ file 'plugincontroller.h'
**
** Created: Sat Dec 8 11:25:50 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/plugincontroller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'plugincontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PluginController[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   18,   17,   17, 0x0a,
      49,   17,   17,   17, 0x0a,
      57,   17,   17,   17, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PluginController[] = {
    "PluginController\0\0text\0textInputEntered(QString)\0"
    "start()\0triggered()\0"
};

void PluginController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PluginController *_t = static_cast<PluginController *>(_o);
        switch (_id) {
        case 0: _t->textInputEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->start(); break;
        case 2: _t->triggered(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PluginController::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PluginController::staticMetaObject = {
    { &DesignTool::staticMetaObject, qt_meta_stringdata_PluginController,
      qt_meta_data_PluginController, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PluginController::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PluginController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PluginController::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PluginController))
        return static_cast<void*>(const_cast< PluginController*>(this));
    return DesignTool::qt_metacast(_clname);
}

int PluginController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DesignTool::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
