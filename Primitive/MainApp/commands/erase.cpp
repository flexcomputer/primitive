#include "erase.h"
#include "drawingview.h"

void EraseTool::start()
{
    // If objects are selected, delete them and exit the command
    deletedObjects = false;
    objectsDeleted = 0;
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            // Delete the object and set the "deletedObjects" flag
            _objects->removeAt(
                        _objects->indexOf(object));
            delete object;
            objectsDeleted++;
            deletedObjects = true;
            _drawingView->documentModified();
        }
    }

    // Check if there were objects deleted
    if (deletedObjects)
    {
        // Terminate the command and display the status in the crosshair box
        _crosshair->setPrompt(tr("Deleted %1 object(s)", "", objectsDeleted).arg(objectsDeleted));
        _drawingView->documentModified(tr("Deleted %1 object(s)", "", objectsDeleted).arg(objectsDeleted));
        _crosshair->setShowPrompt();
        emit stop();
        return;
    }
    if (!deletedObjects)
    {
        singleObjectSelection = false;
        // Set the command window properly
        _commandWindow->setCommandName(tr("Erase"));
        _commandWindow->setPrompt(tr("Select objects to erase or [Cancel/Enter]"));
        // Set the crosshair to the select object state
        _crosshair->setState(Crosshair::SelectObject);
        _crosshair->setPrompt(tr("Select objects to erase"));
        _crosshair->setShowPrompt();
    }
}

void EraseTool::triggered()
{
    // Start the command
    _commandWindow->startCommand(this);
}

void EraseTool::textInputEntered(QString text)
{
    // Check if the user typed cancel
    if (text.toUpper() == tr("C") ||
        text.toUpper() == tr("CANCEL"))
    {
        // Cancel the command
        clearSelection();
        _crosshair->setPrompt(tr(""));
        emit stop();
        _crosshair->setShowPrompt(false);
        return;
    }
}

void EraseTool::paintOverlay(QPainter *painter)
{
    this->selectPaintOverlay(painter);
}

void EraseTool::keyReleaseEvent(QKeyEvent *e)
{
    this->selectKeyReleaseEvent(e);
    // Check for the Enter key
    if (e->key() == Qt::Key_Return)
    {
        // Delete the objects, clear the selection and return
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected())
            {
                // Delete the object and set the "deletedObjects" flag
                _objects->removeAt(
                            _objects->indexOf(object));
                delete object;
                objectsDeleted++;
                deletedObjects = false;
                _drawingView->documentModified();
            }
        }
        // Terminate the command and display the status in the crosshair box
        _crosshair->setPrompt(tr("Deleted %1 object(s)", "", objectsDeleted).arg(objectsDeleted));
        _drawingView->documentModified(tr("Deleted %1 object(s)", "", objectsDeleted).arg(objectsDeleted));
        _crosshair->setShowPrompt();
        emit stop();
        return;
    }
    if (e->key() == Qt::Key_Escape)
    {
        // End the command
        clearSelection();
        _crosshair->setShowPrompt(false);
        emit stop();
        return;
    }
}

void EraseTool::mousePressEvent(QMouseEvent *e)
{
    this->selectMousePressEvent(e);
}

void EraseTool::mouseReleaseEvent(QMouseEvent *e)
{
    this->selectMouseReleaseEvent(e);
    _crosshair->setState(Crosshair::SelectObject);
}
