/****************************************************************************
** Meta object code from reading C++ file 'propertiesinspector.h'
**
** Created: Sun Dec 9 16:52:31 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/propertiesinspector.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'propertiesinspector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_InspectorWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_InspectorWidget[] = {
    "InspectorWidget\0"
};

void InspectorWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData InspectorWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject InspectorWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_InspectorWidget,
      qt_meta_data_InspectorWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &InspectorWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *InspectorWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *InspectorWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_InspectorWidget))
        return static_cast<void*>(const_cast< InspectorWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int InspectorWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_PropertiesInspector[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x0a,
      49,   40,   20,   20, 0x0a,
      80,   71,   20,   20, 0x0a,
     115,  102,   20,   20, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PropertiesInspector[] = {
    "PropertiesInspector\0\0selectionChanged()\0"
    "newColor\0colorChanged(QString)\0newWidth\0"
    "lineWidthChanged(int)\0newLineStyle\0"
    "lineStyleChanged(QString)\0"
};

void PropertiesInspector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PropertiesInspector *_t = static_cast<PropertiesInspector *>(_o);
        switch (_id) {
        case 0: _t->selectionChanged(); break;
        case 1: _t->colorChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->lineWidthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->lineStyleChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PropertiesInspector::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PropertiesInspector::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PropertiesInspector,
      qt_meta_data_PropertiesInspector, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PropertiesInspector::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PropertiesInspector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PropertiesInspector::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PropertiesInspector))
        return static_cast<void*>(const_cast< PropertiesInspector*>(this));
    return QObject::qt_metacast(_clname);
}

int PropertiesInspector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
