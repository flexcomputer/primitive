#ifndef QUIT_H
#define QUIT_H

#include <QAction>
#include "designtool.h"
#include <QIcon>

class QuitTool : public DesignTool
{
    Q_OBJECT
public:
    QuitTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("&Quit"), _mainWindow );
        _action->setShortcut(QKeySequence::Quit);
        _action->setStatusTip(tr("Quits the application"));
        _action->setIcon(QIcon(":/Icons/Close.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("QUIT");
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // QUIT_H
