#ifndef MOVE_H
#define MOVE_H

#include "../designtool.h"
#include <QAction>

const int SELECT_OBJECTS = 0;
const int PICK_BASE_POINT = 1;
const int PICK_DEST_POINT = 2;

class MoveTool : public DesignTool
{
    Q_OBJECT
public:
    MoveTool(DrawingView* view) :
    DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Move"), _mainWindow );
        _action->setStatusTip(tr("Moves one or more objects"));
        _action->setIcon(QIcon(":/Icons/Move.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("MOVE");
    }
    void start();
    void triggered();

    void paintOverlay(QPainter *painter);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    bool highlightObjects() { return stage == SELECT_OBJECTS; }

signals:

public slots:
    void textInputEntered(QString text);

protected:
    int stage;
    bool displacement;
    QVector3D basePoint;
    QVector3D destPoint;

    void moveToPickBasePoint();
    void moveToPickDestPoint();
    void finishCommand();
};

#endif // MOVE_H
