/****************************************************************************
** Meta object code from reading C++ file 'undo.h'
**
** Created: Sat Dec 8 11:24:07 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/commands/undo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'undo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_UndoTool[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,
      18,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_UndoTool[] = {
    "UndoTool\0\0start()\0triggered()\0"
};

void UndoTool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        UndoTool *_t = static_cast<UndoTool *>(_o);
        switch (_id) {
        case 0: _t->start(); break;
        case 1: _t->triggered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData UndoTool::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject UndoTool::staticMetaObject = {
    { &DesignTool::staticMetaObject, qt_meta_stringdata_UndoTool,
      qt_meta_data_UndoTool, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UndoTool::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UndoTool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UndoTool::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UndoTool))
        return static_cast<void*>(const_cast< UndoTool*>(this));
    return DesignTool::qt_metacast(_clname);
}

int UndoTool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DesignTool::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
