#ifndef LINE_H
#define LINE_H

#include "Line_global.h"
#include <QtGui/QStylePlugin>
#include "../PluginSDK/interfaces.h"

#include <QMessageBox>

const int STAGE_PICK_FIRST_POINT=0;
const int STAGE_PICK_SECOND_POINT=1;

class LINESHARED_EXPORT LineToolPlugin : public QObject, public DesignToolPlugin
{
    Q_OBJECT
    Q_INTERFACES(DesignToolPlugin)
public:
    virtual ~LineToolPlugin() { }
    virtual void setDrawingView(DrawingView* view)
    {
        _drawingView = view;
        _objects = &view->objects;
        _crosshair = &view->crosshair;
        _statusBar = view->statusBar;
        _CS = &view->CS;
        _mainWindow = view->mainWin;
        _view = (QFrameView*)view;
        _commandWindow = view->commandWindow;
        _action = new QAction(tr("eBay.com logo"), this);
        connect(_action, SIGNAL(triggered()), SLOT(start()));
    }
public slots:
    void start()
    {
        _crosshair->setState(Crosshair::PickPoint);
        /*_crosshair->setPrompt(tr("Specify point for eBay logo"));
        _crosshair->setShowPrompt();
        _commandWindow->setCommandName(tr("eBay.com Logo"));
        _commandWindow->setPrompt(tr("Specify point for eBay logo or [Cancel]"));*/
    }
    virtual QAction* action()
    {
        return _action;
    }
    virtual QString mnemonic() { return tr("EBAYLOGO"); }
    virtual DesignToolType type() { return Draw; }
};

#endif // LINE_H
