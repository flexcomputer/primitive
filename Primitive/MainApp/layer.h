#ifndef LAYER_H
#define LAYER_H

#include <QObject>
#include "drawingobject.h"

class Layer : public QObject
{
    Q_OBJECT
public:
    explicit Layer(QObject *parent = 0);
    
    QColor color() const { return _color; }
    LineStyle lineStyle() const { return _lineStyle; }

    void setColor(QColor color) { _color = color; }
    void setLineStyle(LineStyle lineStyle) { _lineStyle = lineStyle; }

protected:
    QColor _color;
    LineStyle _lineStyle;

signals:
    
public slots:
    
};

#endif // LAYER_H
