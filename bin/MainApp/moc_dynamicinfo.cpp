/****************************************************************************
** Meta object code from reading C++ file 'dynamicinfo.h'
**
** Created: Sat Dec 8 11:25:55 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/commands/dynamicinfo.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dynamicinfo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DynamicInfoTool[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x0a,
      29,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DynamicInfoTool[] = {
    "DynamicInfoTool\0\0triggered()\0start()\0"
};

void DynamicInfoTool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DynamicInfoTool *_t = static_cast<DynamicInfoTool *>(_o);
        switch (_id) {
        case 0: _t->triggered(); break;
        case 1: _t->start(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DynamicInfoTool::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DynamicInfoTool::staticMetaObject = {
    { &DesignTool::staticMetaObject, qt_meta_stringdata_DynamicInfoTool,
      qt_meta_data_DynamicInfoTool, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DynamicInfoTool::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DynamicInfoTool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DynamicInfoTool::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DynamicInfoTool))
        return static_cast<void*>(const_cast< DynamicInfoTool*>(this));
    return DesignTool::qt_metacast(_clname);
}

int DynamicInfoTool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DesignTool::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
