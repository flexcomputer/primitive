#include "selectall.h"
#include "drawingview.h"

void SelectAllTool::start()
{
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        object->setSelected();
    }
}

void SelectAllTool::triggered()
{
    start();
}
