#include "osnap.h"
#include "settings.h"

void OSnapTool::start()
{
    settings.setSnapEnabled(!settings.snapEnabled());
}

void OSnapTool::triggered()
{
    // Don't stop other commands (e.g. LINE)
    start();
}
