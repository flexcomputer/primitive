/****************************************************************************
** Meta object code from reading C++ file 'maindesigntool.h'
**
** Created: Sat Dec 8 11:23:51 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/maindesigntool.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'maindesigntool.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainDesignTool[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   16,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      54,   49,   15,   15, 0x0a,
      80,   15,   15,   15, 0x0a,
      88,   16,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainDesignTool[] = {
    "MainDesignTool\0\0tool\0commandStarted(DesignTool*)\0"
    "text\0textInputEntered(QString)\0start()\0"
    "commandStartedSlot(DesignTool*)\0"
};

void MainDesignTool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainDesignTool *_t = static_cast<MainDesignTool *>(_o);
        switch (_id) {
        case 0: _t->commandStarted((*reinterpret_cast< DesignTool*(*)>(_a[1]))); break;
        case 1: _t->textInputEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->start(); break;
        case 3: _t->commandStartedSlot((*reinterpret_cast< DesignTool*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainDesignTool::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainDesignTool::staticMetaObject = {
    { &DesignTool::staticMetaObject, qt_meta_stringdata_MainDesignTool,
      qt_meta_data_MainDesignTool, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainDesignTool::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainDesignTool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainDesignTool::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainDesignTool))
        return static_cast<void*>(const_cast< MainDesignTool*>(this));
    return DesignTool::qt_metacast(_clname);
}

int MainDesignTool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DesignTool::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void MainDesignTool::commandStarted(DesignTool * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
