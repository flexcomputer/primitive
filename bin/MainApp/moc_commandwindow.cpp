/****************************************************************************
** Meta object code from reading C++ file 'commandwindow.h'
**
** Created: Thu Nov 8 19:14:27 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/commandwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'commandwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CommandPrompt[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   15,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_CommandPrompt[] = {
    "CommandPrompt\0\0text\0inputEntered(QString)\0"
    "returnPressed()\0"
};

void CommandPrompt::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CommandPrompt *_t = static_cast<CommandPrompt *>(_o);
        switch (_id) {
        case 0: _t->inputEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->returnPressed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CommandPrompt::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CommandPrompt::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CommandPrompt,
      qt_meta_data_CommandPrompt, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CommandPrompt::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CommandPrompt::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CommandPrompt::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CommandPrompt))
        return static_cast<void*>(const_cast< CommandPrompt*>(this));
    return QWidget::qt_metacast(_clname);
}

int CommandPrompt::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void CommandPrompt::inputEntered(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_CommandWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   15,   14,   14, 0x05,
      53,   42,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      81,   14,   14,   14, 0x08,
      97,   15,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CommandWindow[] = {
    "CommandWindow\0\0text\0inputEntered(QString)\0"
    "designTool\0commandStarted(DesignTool*)\0"
    "returnPressed()\0textEntered(QString)\0"
};

void CommandWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CommandWindow *_t = static_cast<CommandWindow *>(_o);
        switch (_id) {
        case 0: _t->inputEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->commandStarted((*reinterpret_cast< DesignTool*(*)>(_a[1]))); break;
        case 2: _t->returnPressed(); break;
        case 3: _t->textEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CommandWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CommandWindow::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CommandWindow,
      qt_meta_data_CommandWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CommandWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CommandWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CommandWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CommandWindow))
        return static_cast<void*>(const_cast< CommandWindow*>(this));
    return QObject::qt_metacast(_clname);
}

int CommandWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void CommandWindow::inputEntered(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CommandWindow::commandStarted(DesignTool * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
