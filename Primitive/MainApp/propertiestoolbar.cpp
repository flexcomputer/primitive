#include "propertiestoolbar.h"
#include <QColorDialog>

PropertiesToolBar::PropertiesToolBar(ObjectList objectList, QObject *parent) :
    QObject(parent)
{
    _objectList = objectList;
    // Create the toolbar
    _colorCombo = new QComboBox();
    // Add the basic colors
    _colorCombo->addItem(tr("Black/White"));
    _colorCombo->addItem(tr("Red"));
    _colorCombo->addItem(tr("Yellow"));
    _colorCombo->addItem(tr("Green"));
    _colorCombo->addItem(tr("Blue"));
    _colorCombo->addItem(tr("Magenta"));
    _colorCombo->addItem(tr("Other..."));
    connect(_colorCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(colorChanged(QString)));
    _lineStyleCombo = new QComboBox();
    _lineStyleCombo->addItem(tr("Solid"));
    _lineStyleCombo->addItem(tr("Dash"));
    _lineStyleCombo->addItem(tr("Dense Dash"));
    _lineStyleCombo->addItem(tr("Dot"));
    _lineStyleCombo->addItem(tr("Dash Dot Dash"));
    connect(_lineStyleCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(lineStyleChanged(QString)));
    _lineWidthCombo = new QComboBox();
    for (int i = 1; i <= 16; i++)
    {
        _lineWidthCombo->addItem(tr("%1").arg(i));
    }
    connect(_lineWidthCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(lineWidthChanged(int)));
    _lineWidthCombo->resize(64, _lineWidthCombo->height());
    _toolBar = new QToolBar(tr("Properties"));
    _toolBar->addWidget(_colorCombo);
    _toolBar->addWidget(_lineStyleCombo);
    _toolBar->addWidget(_lineWidthCombo);
    _defaultColor = Qt::white;
    _defaultLineStyle = Continuous;
    _defaultLineWidth = 1;
}

void PropertiesToolBar::selectionChanged()
{
    // Find the color of the selection
    QColor color = defaultColor();
    bool colorVaries = false;
    LineStyle lineStyle = defaultLineStyle();
    bool lineStyleVaries = false;
    int lineWidth = defaultLineWidth();
    bool lineWidthVaries = false;
    bool firstObject = true; // True for now
    Q_FOREACH(DrawingObject* object, *_objectList)
    {
        if (object->selected())
        {
            // Check if we are the first object
            if (firstObject)
            {
                firstObject = false;
                color = object->color();
                lineStyle = object->lineStyle();
                lineWidth = object->lineWidth();
            }
            else
            {
                if (object->color() != color)
                    // The color varies among the selection
                    // Display *VARIES*
                    colorVaries = true;
                if (object->lineStyle() != lineStyle)
                    lineStyleVaries = true;
                if (object->lineWidth() != lineWidth)
                    lineWidthVaries = true;
            }
        }
    }
    if (colorVaries)
    {
        _colorCombo->addItem(tr("*VARIES*"));
        _colorCombo->setCurrentIndex(_colorCombo->findText(tr("*VARIES*")));
    }
    else
    {
        _colorCombo->setCurrentIndex(_colorCombo->findText(colorText(color)));
        _colorCombo->removeItem(_colorCombo->findText(tr("*VARIES*")));
    }
    if (lineStyleVaries)
    {
        _lineStyleCombo->addItem(tr("*VARIES*"));
        _lineStyleCombo->setCurrentIndex(_lineStyleCombo->findText(tr("*VARIES*")));
    }
    else
    {
        _lineStyleCombo->setCurrentIndex(_lineStyleCombo->findText(lineStyleText(lineStyle)));
        _lineStyleCombo->removeItem(_lineStyleCombo->findText(tr("*VARIES*")));
    }
    if (lineWidthVaries)
    {
        _lineWidthCombo->addItem(tr("*VARIES*"));
        _lineWidthCombo->setCurrentIndex(_lineWidthCombo->findText(tr("*VARIES*")));
    }
    else
    {
        _lineWidthCombo->setCurrentIndex(_lineWidthCombo->findText(tr("%1").arg(lineWidth)));
        _lineWidthCombo->removeItem(_lineWidthCombo->findText(tr("*VARIES*")));
    }
}

void PropertiesToolBar::colorChanged(QString newColor)
{
    if (newColor == tr("Black/White"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(Qt::white);
        }
        _defaultColor = Qt::white;
    }
    if (newColor == tr("Red"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(Qt::red);
        }
        _defaultColor = Qt::red;
    }
    if (newColor == tr("Yellow"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(Qt::yellow);
        }
        _defaultColor = Qt::yellow;
    }
    if (newColor == tr("Green"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(Qt::green);
        }
        _defaultColor = Qt::green;
    }
    if (newColor == tr("Blue"))
    {
        QColor color(50, 200, 255);
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(color);
        }
        _defaultColor = color;
    }
    if (newColor == tr("Magenta"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(Qt::magenta);
        }
        _defaultColor = Qt::magenta;
    }
    if (newColor == tr("Other..."))
    {
        QColor newColor = QColorDialog::getColor(defaultColor());
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setColor(newColor);
            object->setSelected(false);
        }
        _defaultColor = newColor;
    }
}

void PropertiesToolBar::lineStyleChanged(QString newLineStyle)
{
    if (newLineStyle == tr("Solid"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setLineStyle(Continuous);
        }
        _defaultLineStyle = Continuous;
    }
    if (newLineStyle == tr("Dash"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setLineStyle(Dashed);
        }
        _defaultLineStyle = Dashed;
    }
    if (newLineStyle == tr("Dense Dash"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setLineStyle(DashedDense);
        }
        _defaultLineStyle = DashedDense;
    }
    if (newLineStyle == tr("Dot"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setLineStyle(Dotted);
        }
        _defaultLineStyle = Dotted;
    }
    if (newLineStyle == tr("Dash Dot Dash"))
    {
        Q_FOREACH(DrawingObject* object, *_objectList)
        {
            if (object->selected()) object->setLineStyle(DashDotDash);
        }
        _defaultLineStyle = DashDotDash;
    }
}

void PropertiesToolBar::lineWidthChanged(int newWidth)
{
    int width = _lineWidthCombo->itemText(newWidth).toInt();
    Q_FOREACH(DrawingObject* object, *_objectList)
    {
        if (object->selected()) object->setLineWidth(width);
    }
    _defaultLineWidth = width;
}

QString PropertiesToolBar::colorText(QColor color)
{
    if (color == Qt::white)
        return tr("Black/White");
    if (color == Qt::red)
        return tr("Red");
    if (color == Qt::yellow)
        return tr("Yellow");
    if (color == Qt::green)
        return tr("Green");
    if (color == QColor(50, 200, 255))
        return tr("Blue");
    if (color == Qt::magenta)
        return tr("Magenta");
    else
    {
        // Form a custom string
        return tr("(%1, %1, %1)").arg(
                    color.red()).arg(color.green()).arg(color.blue());
    }
}

QString PropertiesToolBar::lineStyleText(LineStyle lineStyle)
{
    if (lineStyle == Continuous)
        return tr("Solid");
    if (lineStyle == Dashed)
        return tr("Dash");
    if (lineStyle == DashedDense)
        return tr("Dense Dash");
    if (lineStyle == Dotted)
        return tr("Dot");
    if (lineStyle == DashDotDash)
        return tr("Dash Dot Dash");
    return "";
}

QString PropertiesToolBar::lineWidthText(int width)
{
    return tr("%1").arg(width);
}
