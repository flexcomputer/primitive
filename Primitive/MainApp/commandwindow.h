#ifndef COMMANDWINDOW_H
#define COMMANDWINDOW_H

#include <QObject>
#include <QDockWidget>
#include <QLineEdit>
#include <QLabel>

class DesignTool;

class CommandPrompt : public QWidget
{
    Q_OBJECT
public:
    CommandPrompt();

    void setCommandName(QString name);
    void setPrompt(QString prompt);
    void setDefaultCaption();
    void setFocus();

protected:
    QLabel* promptLabel;
    QLineEdit* inputBox;
    QString _prompt;
    QString _commandName;

public slots:
    void returnPressed();

signals:
    void inputEntered(QString text);
};

class CommandWindow : public QObject
{
    Q_OBJECT
public:
    explicit CommandWindow(QWidget* parent);

    QDockWidget* dockWidget() const { return _dockWidget; }
    QWidget* parent() const { return _parent; }
    QLineEdit* lineEdit() const { return _lineEdit; }
    QLabel* promptLabel() const { return _promptLabel; }
    QString commandName() const { return _commandName; }
    QString prompt() const { return _prompt; }

    void setCommandName(QString name);
    void setPrompt(QString prompt);
    void setDefaultCaption();
    void parseCommand(QString text);

    void startCommand(DesignTool* designTool) { emit commandStarted(designTool); }
    void setFocus();

protected:
    QDockWidget* _dockWidget;
    QWidget* _parent;
    QLineEdit* _lineEdit;
    QLabel* _promptLabel;
    QString _commandName;
    QString _prompt;
    CommandPrompt* _commandPrompt;

signals:
    void inputEntered(QString text);
    void commandStarted(DesignTool* designTool);

private slots:
    void returnPressed();
    void textEntered(QString text);

};

#endif // COMMANDWINDOW_H
