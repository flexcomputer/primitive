#include "dynamicinfo.h"

void DynamicInfoTool::start()
{
    settings.setDynamicInfo(!settings.dynamicInfo());
}

void DynamicInfoTool::triggered()
{
    this->start();
}
