/****************************************************************************
** Meta object code from reading C++ file 'designtool.h'
**
** Created: Sat Dec 8 11:24:03 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/designtool.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'designtool.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DesignTool[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,

 // slots: signature, parameters, type, tag, flags
      24,   19,   11,   11, 0x0a,
      50,   11,   11,   11, 0x0a,
      58,   11,   11,   11, 0x0a,
      75,   70,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DesignTool[] = {
    "DesignTool\0\0stop()\0text\0"
    "textInputEntered(QString)\0start()\0"
    "triggered()\0tool\0commandStartedSlot(DesignTool*)\0"
};

void DesignTool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DesignTool *_t = static_cast<DesignTool *>(_o);
        switch (_id) {
        case 0: _t->stop(); break;
        case 1: _t->textInputEntered((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->start(); break;
        case 3: _t->triggered(); break;
        case 4: _t->commandStartedSlot((*reinterpret_cast< DesignTool*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData DesignTool::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DesignTool::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DesignTool,
      qt_meta_data_DesignTool, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DesignTool::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DesignTool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DesignTool::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DesignTool))
        return static_cast<void*>(const_cast< DesignTool*>(this));
    return QObject::qt_metacast(_clname);
}

int DesignTool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void DesignTool::stop()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
