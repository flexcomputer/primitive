#include "new.h"
#include "../drawingview.h"

void NewTool::start()
{
    _drawingView->newDoc();
    emit stop();
}

void NewTool::triggered()
{
    _commandWindow->startCommand(this);
}
