#include "dwgrectangle.h"
#include "settings.h"
#include <QLineF>
#include <QToolBox>
#include "propwidgets/button.h"

DwgRectangle::DwgRectangle(float x1, float y1, float x2, float y2, float z, QColor c)
    :
      DrawingObject(),
      _x1(x1),
      _y1(y1),
      _x2(x2),
      _y2(y2),
      _z(z)
{
    scaling.setX(1); scaling.setY(1); scaling.setZ(1);
    _color = c;
    _lineWidth = 1;
    _linestyle = Continuous;
    createGeometry();
    rotate90Act = new QAction(tr("Switch Orientation"), this);
    connect(rotate90Act, SIGNAL(triggered()), SLOT(rotate90()));
}

void DwgRectangle::createGeometry()
{
    _position.setX((_x1 + _x2)/2);
    _position.setY((_y1 + _y2)/2);
    _position.setZ(_z);
    if (handleArray) delete handleArray;
    // Create the handle array
    handleArray = new Handle[9];
    handleArray[1].move(transformPoint(_x1, _y1, _z), PointHandle);
    handleArray[0].move(transformPoint(_x2, _y1, _z), PointHandle);
    handleArray[2].move(transformPoint(_x2, _y2, _z), PointHandle);
    handleArray[3].move(transformPoint(_x1, _y2, _z), PointHandle);
    handleArray[4].move(transformPoint((_x1 + _x2)/2, _y1, _z), PointHandle);
    handleArray[5].move(transformPoint(_x1, (_y1 + _y2)/2, _z), PointHandle);
    handleArray[6].move(transformPoint(_x2, (_y1 + _y2)/2, _z), PointHandle);
    handleArray[7].move(transformPoint((_x1 + _x2)/2, _y2, _z), PointHandle);
    handleArray[8].move((_x1 + _x2)/2, (_y1 + _y2)/2, _z, PointHandle);
    _handleCount = 9;
    // Snap points
    if (snapArray) delete snapArray;
    // Create the array
    snapArray = new SnapPoint[10];
    snapArray[0].move(transformPoint(_x1, _y1, _z), SnapPoint::Endpoint);
    snapArray[1].move(transformPoint(_x1, _y2, _z), SnapPoint::Endpoint);
    snapArray[2].move(transformPoint(_x2, _y2, _z), SnapPoint::Endpoint);
    snapArray[3].move(transformPoint(_x2, _y1, _z), SnapPoint::Endpoint);
    snapArray[4].move(transformPoint((_x1 + _x2)/2, _y1, _z), SnapPoint::Midpoint);
    snapArray[5].move(transformPoint((_x1 + _x2)/2, _y2, _z), SnapPoint::Midpoint);
    snapArray[6].move(transformPoint(_x1, (_y1 + _y2)/2, _z), SnapPoint::Midpoint);
    snapArray[7].move(transformPoint(_x2, (_y1 + _y2)/2, _z), SnapPoint::Midpoint);
    snapArray[8].move((_x1 + _x2)/2, (_y1 + _y2)/2, _z, SnapPoint::Center);
    _snapCount = 9;
}

void DwgRectangle::render(QFrameView *view, bool highlighted)
{
    // Save the view pointer
    _view = view;
    // Find the rectangle color and line style
    if (highlighted && (_selected == false))
    {
        view->setLineStyle(DashedDense);
        view->setLineWidth(lineWidth() * 2);
    }
    else
    {
        view->setLineStyle(((_selected == false) ? lineStyle() : DashedDense));
        view->setLineWidth(lineWidth());
    }
    QColor rendercolor = (highlighted && (_selected == false) ?
                        QColor(255, 255, 255) :
                        color());
    // Top line
    view->drawLine(_x1, _y1, _z, _x2, _y1, _z, rendercolor);
    // Bottom line
    view->drawLine(_x1, _y2, _z, _x2, _y2, _z, rendercolor);
    // Left line
    view->drawLine(_x1, _y1, _z, _x1, _y2, _z, rendercolor);
    // Right line
    view->drawLine(_x2, _y1, _z, _x2, _y2, _z, rendercolor);
}

QRect DwgRectangle::screenRect() const
{
    // Find the screen coordinates of the points
    QPoint point1 = _view->camera()->physicalPoint(x1(), y1(), z());
    QPoint point2 = _view->camera()->physicalPoint(x2(), y2(), z());
    // Create the rectangle
    QRect box;
    if (point1.x() < point2.x())
    {
        box.setX(point1.x());
        box.setWidth(point2.x() - point1.x());
    }
    else
    {
        box.setX(point2.x());
        box.setWidth(point1.x() - point2.x());
    }
    if (point1.y() < point2.y())
    {
        box.setY(point1.y());
        box.setHeight(point2.y() - point1.y());
    }
    else
    {
        box.setY(point2.y());
        box.setHeight(point1.y() - point2.y());
    }
    // Return the rectangle
    return box;
}

void DwgRectangle::loadFromBinary(QDataStream *stream)
{
    DrawingObject::loadFromBinary(stream);
    // Load the vertices
    (*stream) >> _x1;
    (*stream) >> _x2;
    (*stream) >> _y1;
    (*stream) >> _y2;
    (*stream) >> _z;
    // Create the geometry
    createGeometry();
}

void DwgRectangle::saveToBinary(QDataStream *stream)
{
    DrawingObject::saveToBinary(stream);
    // Write the vertices
    (*stream) << _x1 << _x2 << _y1 << _y2 << _z;
}

bool DwgRectangle::hitTest(QPoint point, QOrthoCamera* camera)
{
    QPoint point1 = camera->physicalPoint(x1(), y1(), z());
    QPoint point2 = camera->physicalPoint(x2(), y2(), z());
    QRect box;
    if (point1.x() < point2.x())
    {
        box.setX(point1.x());
        box.setWidth(point2.x() - point1.x());
    }
    else
    {
        box.setX(point2.x());
        box.setWidth(point1.x() - point2.x());
    }
    if (point1.y() < point2.y())
    {
        box.setY(point1.y());
        box.setHeight(point2.y() - point1.y());
    }
    else
    {
        box.setY(point2.y());
        box.setHeight(point1.y() - point2.y());
    }
    QRect pickBox(point.x() - settings.crossRectSize()/2, point.y() - settings.crossRectSize()/2,
                  settings.crossRectSize(), settings.crossRectSize());
    return box.intersects(pickBox) && !insideRect(point, box);
}

void DwgRectangle::move(float xStart, float yStart, float zStart, float xDest, float yDest, float zDest)
{
    // Find the difference
    float xDiff = xDest - xStart;
    float yDiff = yDest - yStart;
    float zDiff = zDest - zStart;
    _x1 += xDiff;
    _y1 += yDiff;
    _z += zDiff;
    _x2 += xDiff;
    _y2 += yDiff;
    createGeometry();
}

void DwgRectangle::handleMoved(int handle, float x, float y, float z)
{
    QVector3D untransformed = detransformPoint(QVector3D(x, y, z));
    Q_UNUSED(z);
    switch (handle)
    {
    case 1: // Bottom-left corner:
    {
        _x1 = untransformed.x();
        _y1 = untransformed.y();
        break;
    }
    case 0: // Bottom-right corner:
        _x2 = untransformed.x();
        _y1 = untransformed.y();
        break;
    case 2: // Top-right corner:
        _x2 = untransformed.x();
        _y2 = untransformed.y();
        break;
    case 3: // Top-left corner:
        _x1 = untransformed.x();
        _y2 = untransformed.y();
        break;
    case 4: // Top midpoint
        _y1 = untransformed.y();
        break;
    case 7: // Bottom midpoint
        _y2 = untransformed.y();
        break;
    case 6: // Left midpoint
        _x2 = untransformed.x();
        break;
    case 5: // Right midpoint
        _x1 = untransformed.x();
        break;
    case 8: // Center
    {
        float xDiff = x - (_x1 + _x2)/2;
        float yDiff = y - (_y1 + _y2)/2;
        _x1 += xDiff;
        _x2 += xDiff;
        _y1 += yDiff;
        _y2 += yDiff;
        break;
    }
    default:
        break;
    }
    createGeometry();
}

void DwgRectangle::createContextMenu(QMenu *menu)
{
    menu->addAction(rotate90Act);
}

void DwgRectangle::createProperties(QToolBox *toolBox)
{
    // Add the "Rectangle" page
    PropertyGroupWidget* rectangleGroup = new PropertyGroupWidget();
    toolBox->addItem(rectangleGroup, tr("Rectangle"));
    // Add the "Switch Orientation" button
    ButtonProperty* switchOrientationBtn = new ButtonProperty(tr("Switch Orientation"));
    connect(switchOrientationBtn, SIGNAL(clicked()), SLOT(rotate90()));
    rectangleGroup->addWidget(switchOrientationBtn);
    rectangleGroup->endWidgets();
}

void DwgRectangle::rotate90()
{
    // Find the center coordinates
    float xC = (_x1 + _x2)/2;
    float yC = (_y1 + _y2)/2;

    // Move the rectangle to the center
    _x1 -= xC;
    _x2 -= xC;
    _y1 -= yC;
    _y2 -= yC;

    // Do the rotation, if you want to understand this, draw the shape on a piece of paper
    // and verify the method yourself!
    float newX1 = _y1;
    float newX2 = _y2;
    float newY1 = _x1;
    float newY2 = _x2;

    _x1 = newX1;
    _x2 = newX2;
    _y1 = newY1;
    _y2 = newY2;

    // Translate the rectangle back to its original position
    _x1 += xC;
    _x2 += xC;
    _y1 += yC;
    _y2 += yC;

    createGeometry();
    // Repaint it in case the rotation was made with Property Inspector
    _view->repaint();
}
