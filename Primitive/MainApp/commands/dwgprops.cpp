#include "dwgprops.h"
#include "../drawingview.h"
#include "drawingpropertiesdlg.h"

void DwgPropsTool::start()
{
    DrawingPropertiesDlg* drawingDlg = new DrawingPropertiesDlg(_mainWindow,
                                                                _drawingView->fileName);
    drawingDlg->setTitle(tr("Drawing Properties"));
    drawingDlg->exec();
    // Set the drawing title
    _drawingView->fileName = drawingDlg->drawingName();
    emit stop();
}

void DwgPropsTool::triggered()
{
    _commandWindow->startCommand(this);
}
