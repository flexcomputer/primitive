#include "rect.h"
#include "drawingview.h"
#include "objects/dwgrectangle.h"

void RectTool::triggered()
{
    _commandWindow->startCommand(this);
}

void RectTool::start()
{
    drawnRectangle = false;
    moveToPickFirstPoint();
}

void RectTool::moveToPickFirstPoint()
{
    stage = RECT_STAGE_PICK_FIRST_POINT;
    pickPointSetPrompt(tr("Specify first point"));
    _crosshair->setShowPrompt(true);
    _crosshair->setState(Crosshair::PickPoint);
    _commandWindow->setCommandName(tr("Rect"));
    _commandWindow->setPrompt(tr("Specify first point or [Cancel]"));
}

void RectTool::moveToPickSecondPoint()
{
    firstPoint = point;
    lastPoint = point;
    stage = RECT_STAGE_PICK_SECOND_POINT;
    pickPointSetPrompt(tr("Specify second point"));
    _commandWindow->setPrompt(tr("Specify second point or [Cancel]"));
}

void RectTool::addRectangle()
{
    DwgRectangle* rect = new DwgRectangle(firstPoint.x(), firstPoint.y(), point.x(), point.y(), point.z(),
                                          _drawingView->propertiesInspector->defaultColor());
    rect->setLineStyle(_drawingView->propertiesInspector->defaultLineStyle());
    rect->setLineWidth(_drawingView->propertiesInspector->defaultLineWidth());
    rect->createGeometry();
    _drawingView->objects.append(rect);
    _crosshair->setShowPrompt(false);
    emit stop();
    drawnRectangle = true;
    _drawingView->documentModified(tr("Added rectangle"));
}

void RectTool::mouseMoveEvent(QMouseEvent *e)
{
    pickPointMouseMoveEvent(e);
}

void RectTool::mouseReleaseEvent(QMouseEvent *e)
{
    pickPointMouseReleaseEvent(e);
    if (stage == RECT_STAGE_PICK_FIRST_POINT)
        moveToPickSecondPoint();
    else if (stage == RECT_STAGE_PICK_SECOND_POINT)
        addRectangle();
}

void RectTool::paintOverlay(QPainter *painter)
{
    if (stage == RECT_STAGE_PICK_SECOND_POINT)
    {
        // Paint an estimation of the new rectangle
        painter->setPen(_drawingView->propertiesInspector->defaultColor());
        painter->drawRect(
                    QRect(
                        _view->camera()->physicalPoint(firstPoint.x(), firstPoint.y(), firstPoint.z()),
                        _view->camera()->physicalPoint(pointToPick.x(), pointToPick.y(), pointToPick.z())));
    }
    pickPointPaintOverlay(painter);
}

void RectTool::keyReleaseEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
    {
        // Quit the command
        _crosshair->setShowPrompt(false);
        emit stop(); return;
    }
}

void RectTool::textInputEntered(QString text)
{
    if (pickPointInputEntered(text))
    {
        if (stage == RECT_STAGE_PICK_FIRST_POINT)
        {
            moveToPickSecondPoint();
        }
        else if (stage == RECT_STAGE_PICK_SECOND_POINT)
        {
            addRectangle();
        }
    }
    else if (text.toUpper() == tr("CANCEL"))
        emit stop();
}

