#ifndef UCS_H
#define UCS_H

#include "../designtool.h"
#include <QAction>

class CSTool : public DesignTool
{
    Q_OBJECT
public:
    CSTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Move &UCS"), _mainWindow);
        _action->setStatusTip(tr("Moves the UCS allowing you to insert coordinates relative to it"));
        _action->setWhatsThis(_action->statusTip());
        connect(_action, SIGNAL(triggered()), SLOT(triggered()));
        _mnemonic = tr("MOVEUCS");
    }
    
    void paintOverlay(QPainter *painter);
    void keyReleaseEvent(QKeyEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    bool highlightObjects() { return false; }

signals:

public slots:
    void textInputEntered(QString text);
    
public slots:
    void triggered();
    void start();

    void finishCommand();

protected:

};

#endif // UCS_H
