#ifndef DRAWINGOBJECTS_H
#define DRAWINGOBJECTS_H

#include "objects/dwgline.h"
#include "objects/dwgrectangle.h"
#include "objects/dwgellipse.h"

#endif // DRAWINGOBJECTS_H
