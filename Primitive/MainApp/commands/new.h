#ifndef NEW_H
#define NEW_H

#include <QAction>
#include <QStyle>
#include "designtool.h"

class NewTool : public DesignTool
{
    Q_OBJECT
public:
    NewTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("&New"), _mainWindow);
        _action->setStatusTip(tr("Creates a new drawing"));
        _action->setShortcut(QKeySequence::New);
        _action->setIcon(QIcon(":/Icons/New.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("NEW");
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // NEW_H
