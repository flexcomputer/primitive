#ifndef ORTHO_H
#define ORTHO_H

#include "../designtool.h"
#include <QAction>

class OrthoTool : public DesignTool
{
    Q_OBJECT
public:
    OrthoTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("Ortho"), _mainWindow);
        _action->setWhatsThis(tr("Enables ORTHO, allowing you to draw only horizontal and vertical lines"));
        _action->setStatusTip(_action->whatsThis());
        _action->setCheckable(true);
        _action->setChecked(false);
        connect(_action, SIGNAL(changed()), this, SLOT(triggered()));
        _mnemonic = tr("ORTHO");
    }
    
signals:
    
public slots:
    void triggered();
    void start();
    
};

#endif // ORTHO_H
