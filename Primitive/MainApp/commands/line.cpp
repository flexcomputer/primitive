#include "line.h"
#include "drawingview.h"
#include "objects/dwgline.h"

void LineTool::start()
{
    // Set the crosshair prompt
    pickPointSetPrompt(tr("Specify first point"));
    _crosshair->setShowPrompt(true);
    _crosshair->setState(Crosshair::PickPoint);
    _commandWindow->setCommandName(tr("Line"));
    _commandWindow->setPrompt(tr("Specify first point or [Cancel]"));
    stage = STAGE_PICK_FIRST_POINT;
    pickedLastPoint = false;
    lineCreated = false;
}

void LineTool::createdLine()
{
    lineCreated = true;
    _drawingView->documentModified(tr("Added a line"));
}

void LineTool::triggered()
{
    _commandWindow->startCommand(this);
}

void LineTool::paintOverlay(QPainter *painter)
{
    // If we have already selected the previous point, draw a preview of the line
    if (stage == STAGE_PICK_SECOND_POINT)
    {
        painter->setPen(_drawingView->propertiesInspector->defaultColor());
        painter->drawLine(
                    _view->camera()->physicalPoint(lastPoint.x(),
                                                   lastPoint.y(),
                                                   lastPoint.z()),
                    _view->camera()->physicalPoint(pointToPick.x(), pointToPick.y(), pointToPick.z()));
    }
    pickPointPaintOverlay(painter);
}

void LineTool::mouseMoveEvent(QMouseEvent *e)
{
    pickPointMouseMoveEvent(e);
}

void LineTool::mousePressEvent(QMouseEvent *e)
{
    Q_UNUSED(e);
}

void LineTool::mouseReleaseEvent(QMouseEvent *e)
{
    pickPointMouseReleaseEvent(e);
    if (stage == STAGE_PICK_FIRST_POINT)
    {
        goToSecondStage();
    }
    else if (stage == STAGE_PICK_SECOND_POINT)
    {
        addLineSegment();
    }
}

void LineTool::keyReleaseEvent(QKeyEvent *e)
{
    switch (e->key())
    {
        case Qt::Key_Escape:
        case Qt::Key_Cancel:
        case Qt::Key_Pause:
        {
            // Cancel the command
            emit stop();
            _crosshair->setShowPrompt(false);
            break;
        }
        break;
        /*case Qt::Key_Return:
        case Qt::Key_Enter:
        {
            // Finish the command
            emit stop();
            _crosshair->setShowPrompt(false);
            break;
        }*/
    }
}

void LineTool::goToSecondStage()
{
    lastPoint = point;
    pickPointSetPrompt(tr("Specify second point"));
    _commandWindow->setPrompt(tr("Specify second point or [Cancel]"));
    stage = STAGE_PICK_SECOND_POINT;
    pickedLastPoint = true;
    startPoint = point;
}

void LineTool::addLineSegment()
{
    if (lastPoint == point) return; // Don't add unnecessary objects
    // Add a line
    DwgLine* line = new DwgLine(
                lastPoint.x(), lastPoint.y(), lastPoint.z(),
                point.x(), point.y(), point.z(),
                _drawingView->propertiesInspector->defaultColor()); // TODO : Get the color from the current layer
    line->setLineStyle(_drawingView->propertiesInspector->defaultLineStyle());
    line->setLineWidth(_drawingView->propertiesInspector->defaultLineWidth());
    _objects->append(line);
    previousPoint = lastPoint;
    // Make the current point the previous one
    lastPoint = point;
    // Update the prompts
    pickPointSetPrompt(tr("Specify end point"));
    _commandWindow->setPrompt(tr("Select next point or [Cancel/Enter]"));
    // The document has been modified
    createdLine();
}

void LineTool::textInputEntered(QString text)
{
    // Call the standard pick point function
    if (pickPointInputEntered(text))
    {
        // The user entered a point
        if (stage == STAGE_PICK_FIRST_POINT)
            goToSecondStage();
        if (stage == STAGE_PICK_SECOND_POINT)
            addLineSegment();
    }
    else
    {
        if (text.toUpper() == tr("C") || text.toUpper() == tr("CLOSE"))
        {
            closeLine();
        }
        // End the command
        emit stop();
        _crosshair->setShowPrompt(false);
    }
}

void LineTool::cancel()
{
    _crosshair->setShowPrompt(false);
    emit stop();
    return;
}

void LineTool::closeLine()
{
    // Close the line
    DwgLine* line = new DwgLine(
                point.x(), point.y(), point.z(),
                startPoint.x(), startPoint.y(), startPoint.z(),
                _drawingView->propertiesInspector->defaultColor()); // TODO : Get the color from the current layer
    line->setLineStyle(_drawingView->propertiesInspector->defaultLineStyle());
    line->setLineWidth(_drawingView->propertiesInspector->defaultLineWidth());
    _objects->append(line);
    createdLine();
    cancel();
}
