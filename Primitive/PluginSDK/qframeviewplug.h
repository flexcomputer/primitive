// Primitive Plugin SDK
// Copyright (C) Flex Computer 2012 - All rights reserved

#ifndef QFRAMEVIEW_H
#define QFRAMEVIEW_H

#include <QGLWidget>
#include <QVector3D>

#define PI 3.1415926535897 // This should be precise enough :-)

class QCamera
{
public:
    QCamera() { }
    QCamera(float centerX, float centerY, float centerZ,
            float eyeX, float eyeY, float eyeZ,
            float upX, float upY, float upZ) :
        _centerX(centerX),
        _centerY(centerY),
        _centerZ(centerZ),
        _eyeX(eyeX),
        _eyeY(eyeY),
        _eyeZ(eyeZ),
        _upX(upX),
        _upY(upY),
        _upZ(upZ)
    { }

    QCamera(QVector3D center, QVector3D eye, QVector3D up) :
        _centerX(center.x()),
        _centerY(center.y()),
        _centerZ(center.z()),
        _eyeX(eye.x()),
        _eyeY(eye.y()),
        _eyeZ(eye.z()),
        _upX(up.x()),
        _upY(up.y()),
        _upZ(up.z())
    { }

    float centerX() const { return _centerX; }
    float centerY() const { return _centerY; }
    float centerZ() const { return _centerZ; }
    QVector3D center() const
    {
        QVector3D _center(_centerX, _centerY, _centerZ);
        return _center;
    }

    float eyeX() const { return _eyeX; }
    float eyeY() const { return _eyeY; }
    float eyeZ() const { return _eyeZ; }
    QVector3D eye() const
    {
        QVector3D _eye(_eyeX, _eyeY, _eyeZ);
        return _eye;
    }

    float upX() const { return _upX; }
    float upY() const { return _upY; }
    float upZ() const { return _upZ; }
    QVector3D up() const
    {
        QVector3D _up(_upX, _upY, _upZ);
        return _up;
    }

    void setCenterX(float x) { _centerX = x; }
    void setCenterY(float y) { _centerY = y; }
    void setCenterZ(float z) { _centerZ = z; }
    void setCenter(QVector3D center)
    {
        _centerX = center.x();
        _centerY = center.y();
        _centerZ = center.z();
    }

    void setEyeX(float x) { _eyeX = x; }
    void setEyeY(float y) { _eyeY = y; }
    void setEyeZ(float z) { _eyeZ = z; }
    void setEye(QVector3D eye)
    {
        _eyeX = eye.x();
        _eyeY = eye.y();
        _eyeZ = eye.z();
    }

    void setUpX(float x) { _upX = x; }
    void setUpY(float y) { _upY = y; }
    void setUpZ(float z) { _upZ = z; }
    void setUp(QVector3D up)
    {
        _upX = up.x();
        _upY = up.y();
        _upZ = up.z();
    }

    // API to be reimplemented by children classes
    virtual void resizeScene(int width, int height)
    {
        Q_UNUSED(width);
        Q_UNUSED(height);
        // Nothing for the base class
    }

    virtual void zoom(int times) { Q_UNUSED(times); }

protected:
    float _centerX, _centerY, _centerZ;
    float _eyeX, _eyeY, _eyeZ;
    float _upX, _upY, _upZ;
};

class QOrthoCamera : public QCamera
{
public:
    QOrthoCamera() { }

public:
    void resizeScene(int width, int height)
    { Q_UNUSED(width); Q_UNUSED(height); }
    void zoom(int times) { Q_UNUSED(times); }
    void setViewport2D(float _left, float _top, float _right, float _bottom)
    { Q_UNUSED(_left); Q_UNUSED(_right); Q_UNUSED(_top); Q_UNUSED(_bottom); }
    QPoint physicalPoint(float x, float y, float z)
    { Q_UNUSED(x); Q_UNUSED(y); Q_UNUSED(z); }
    void logicalPoint2D(QPoint point, float& x, float& y, float& z)
    { Q_UNUSED(point); Q_UNUSED(x); Q_UNUSED(y); Q_UNUSED(z); }

    float top, left, right, bottom;
    int _width, _height;
};

// Projection types
enum ProjectionType
{
    Orthographic2D = 0,
    Orthographic3D = 0,
    Perspective = 0
};

enum LineStyle
{
    Continuous,
    Dashed,
    DashedDense,
    Dotted,
    DashDotDash,
    DotDashDot
};

enum PolygonMode
{
    Point,
    Fill,
    Wireframe
};

enum PolygonFace
{
    Front,
    Back,
    All
};

class QFrameView : public QGLWidget
{
    Q_OBJECT
public:
    explicit QFrameView(QWidget *parent = 0);

    virtual void initializeGL() { }
    virtual void resizeGL(int width, int height) { Q_UNUSED(width); Q_UNUSED(height); }
    virtual void paintEvent(QPaintEvent *) { }
    virtual void paintGL() { }
    virtual void paintOverlay(QPainter& painter) { Q_UNUSED(painter); }

    // QFrame API
    // Drawing functions
    void drawPoint(float x, float y, float z, QColor color)
    {
        Q_UNUSED(x); Q_UNUSED(y); Q_UNUSED(z); Q_UNUSED(color);
    }

    void drawLine(float x1, float y1, float z1, float x2, float y2, float z2, QColor color)
    {
        Q_UNUSED(x1); Q_UNUSED(y1); Q_UNUSED(z1);
        Q_UNUSED(x2); Q_UNUSED(y2); Q_UNUSED(z2);
        Q_UNUSED(color);
    }

    void drawTriangle(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, QColor color)
    {
        Q_UNUSED(x1); Q_UNUSED(y1); Q_UNUSED(z1);
        Q_UNUSED(x2); Q_UNUSED(y2); Q_UNUSED(z2);
        Q_UNUSED(x3); Q_UNUSED(y3); Q_UNUSED(z3);
        Q_UNUSED(color);
    }

    void drawQuad(float x1, float y1, float z1,
                  float x2, float y2, float z2,
                  float x3, float y3, float z3,
                  float x4, float y4, float z4,
                  QColor color)
    {
        Q_UNUSED(x1); Q_UNUSED(y1); Q_UNUSED(z1);
        Q_UNUSED(x2); Q_UNUSED(y2); Q_UNUSED(z2);
        Q_UNUSED(x3); Q_UNUSED(y3); Q_UNUSED(z3);
        Q_UNUSED(x4); Q_UNUSED(y4); Q_UNUSED(z4);
        Q_UNUSED(color);
    }

    void drawCircle(float x, float y, float radius, QColor color = Qt::white, int segmentCount = 100)
    {
        Q_UNUSED(x); Q_UNUSED(y); Q_UNUSED(radius);
        Q_UNUSED(color); Q_UNUSED(segmentCount);
    }

    void drawGrid(float size, float step, QColor gridColor, QColor gridHighColor,
                  QColor xAxisColor, QColor yAxisColor, QColor zAxisColor,
                  bool drawAxes, bool drawFullAxes)
    {
        Q_UNUSED(size); Q_UNUSED(step); Q_UNUSED(gridColor); Q_UNUSED(gridHighColor);
        Q_UNUSED(xAxisColor); Q_UNUSED(yAxisColor); Q_UNUSED(zAxisColor);
        Q_UNUSED(drawAxes); Q_UNUSED(drawFullAxes);
    }

    void rotationStart(float angleX, float angleY, float angleZ, float centerX, float centerY, float centerZ)
    {
        Q_UNUSED(angleX); Q_UNUSED(angleY); Q_UNUSED(angleZ);
        Q_UNUSED(centerX); Q_UNUSED(centerY); Q_UNUSED(centerZ);
    }

    void rotationEnd() { }

    // Get/Set functions
    QOrthoCamera* camera() const { return _camera; }

    void setCamera(QOrthoCamera* camera)
    {
        if (_camera) delete _camera;
        _camera = camera;
    }
    void setLineWidth(float width) { glLineWidth(width); }

    void setLineStyle(LineStyle style) { Q_UNUSED(style); }

    void setPolygonMode(PolygonFace face, PolygonMode mode)
    {
        Q_UNUSED(face); Q_UNUSED(mode);
    }

    void setLineStipple(int factor, unsigned short pattern) { glLineStipple(factor, pattern); }

    void setPolygonStipple(const unsigned char* pattern) { glPolygonStipple(pattern); }

    void setFillPolygons(bool fill = true) { glPolygonMode(GL_FRONT_AND_BACK, (fill ? GL_FILL : GL_LINE)); }

protected:
    QOrthoCamera* _camera;

signals:

public slots:

};

#endif // QFRAMEVIEW_H
