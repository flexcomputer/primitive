#ifndef GRID_H
#define GRID_H

#include <QAction>
#include "designtool.h"
#include "settings.h"

class GridTool : public DesignTool
{
    Q_OBJECT
public:
    GridTool(DrawingView* view) :
    DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("&Grid"), _mainWindow );
        _action->setStatusTip(tr("Shows or hides the grid"));
        connect(_action, SIGNAL(changed()), this, SLOT(triggered()));
        _action->setCheckable(true);
        _action->setChecked(true);
        // _action->setChecked(settings.showGrid());
        // Set the mnemonic
        _mnemonic = tr("GRID");
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // GRID_H
