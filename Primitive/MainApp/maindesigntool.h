#ifndef MAINDESIGNTOOL_H
#define MAINDESIGNTOOL_H

#include "designtool.h"

class MainDesignTool : public DesignTool
{
private:
    Q_OBJECT
public:
    MainDesignTool(DrawingView* drawingView) :
        DesignTool(drawingView),
        draggingHandle(false), useMainTool(true)
    {
        // Link the command window, start command action to our class
        connect(_commandWindow, SIGNAL(commandStarted(DesignTool*)), this, SLOT(commandStartedSlot(DesignTool*)));
        disconnect(_commandWindow, SIGNAL(inputEntered(QString)));
        connect(_commandWindow, SIGNAL(inputEntered(QString)), this, SLOT(textInputEntered(QString)));
        _currentTool = this;
    }

    virtual void mousePressEvent(QMouseEvent* e);
    virtual void mouseReleaseEvent(QMouseEvent* e);
    virtual void mouseMoveEvent(QMouseEvent *e);
    virtual void keyPressEvent(QKeyEvent* e);
    virtual void keyReleaseEvent(QKeyEvent* e);
    virtual void paintOverlay(QPainter *painter);
    virtual void paintGL();
    virtual bool highlightObjects() { return !dragging; }

    QList<DesignTool*>* commandList() { return &_commandList; }

    void addCommand(DesignTool* designTool) { _commandList.insert(_commandList.count(), designTool); }
    void startCommand(DesignTool* tool)
    {
        useMainTool = false;
        _currentTool = tool;
        connect(tool, SIGNAL(stop()), this, SLOT(start())); // Looks tricky but it makes sense after a while
        tool->start();
    }

    void showContextMenu();

protected:
    bool draggingHandle;
    DrawingObject* handleObject;
    QPoint handleDragStart;
    int handleId;

    // The command list
    QList<DesignTool*> _commandList;
public:
    DesignTool* _currentTool;

    bool useMainTool;

signals:
    void commandStarted(DesignTool* tool);

public slots:
    virtual void textInputEntered(QString text);
    void start();
    virtual void commandStartedSlot(DesignTool *tool)  { startCommand(tool); }
};

#endif // MAINDESIGNTOOL_H
