// Primitive Plugin SDK
// Copyright (C) Flex Computer - All rights reserved

#ifndef CROSSHAIR_H
#define CROSSHAIR_H

#include <QObject>
#include <QPainter>

class Crosshair : public QObject
{
    Q_OBJECT
public:
    explicit Crosshair(QObject *parent = 0) { }

    // States
    enum State {
        MainSelect = 0,
        PickPoint = 1,
        SelectObject = 2,
        Hidden = 3
    };

    // Get/set functions
    virtual int x() const { return _x; }
    virtual int y() const { return _y; }
    virtual QString prompt() const { return _prompt; }
    virtual State state() const { return _state; }
    virtual bool showPrompt() const { return _showPrompt; }

    virtual void setX(int x) { _x = x; }
    virtual void setY(int y) { _y = y; }
    virtual void setPrompt(QString prompt) { _prompt = prompt; }
    virtual void setState(State state) { _state = state; }
    virtual void setShowPrompt(bool show = true) { _showPrompt = show; }

    virtual void paintAt(QPainter& painter) { Q_UNUSED(painter); }

protected:
    int _x, _y; // The position of the crosshair
    QString _prompt; // The crosshair's prompt
    State _state; // The crosshair's state
    bool _showPrompt; // True - draw the prompt

signals:

public slots:

};

#endif CROSSHAIR_H
