#ifndef ABOUT_H
#define ABOUT_H

#include "designtool.h"
#include <QAction>

class AboutTool : public DesignTool
{
    Q_OBJECT
public:
    AboutTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("&About..."), _mainWindow );
        _action->setStatusTip(tr("Displays version information about this software"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("ABOUT");
    }
    
signals:
    
public slots:
    void triggered();
    void start();
    
};

#endif // ABOUT_H
