#ifndef ROTATE_H
#define ROTATE_H

#include "../designtool.h"

// See Docs/Rotate.md

// Stages
#define ROTATE_SELECT_OBJECTS 0
#define ROTATE_SPECIFY_BASE_POINT 1
#define ROTATE_SPECIFY_ANGLE 2

class RotateTool : public DesignTool
{
    Q_OBJECT
public:
    RotateTool(DrawingView* view);

    bool highlightObjects();

    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void keyReleaseEvent(QKeyEvent *e);

    void paintOverlay(QPainter *painter);

    void textInputEntered(QString text);
    
protected:
    void moveToSelectObjects();
    void moveToSpecifyBasePoint();
    void moveToSpecifyAngle();
    void finishCommand();

    QVector3D basePoint;
    float angle;
    float initialAngle;

    int stage;

signals:
    
public slots:
    void triggered();
    void start();

    void cancel();
    
};

#endif // ROTATE_H
