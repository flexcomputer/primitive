#ifndef SAVEAS_H
#define SAVEAS_H

#include "../designtool.h"
#include <QAction>
#include <QFileDialog>
#include <QKeySequence>

class SaveAsTool : public DesignTool
{
    Q_OBJECT
public:
    SaveAsTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("Save &As..."), _mainWindow);
        _action->setStatusTip(tr("Saves the drawing with a new name"));
        _action->setShortcut(QKeySequence::SaveAs);
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("SAVEAS");
        // Add additional construction code here
    }
    
signals:
    
public slots:
    void start();
    void triggered();
    
};

#endif // SAVEAS_H
