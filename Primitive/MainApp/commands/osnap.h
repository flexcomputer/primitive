#ifndef OSNAP_H
#define OSNAP_H

#include "../designtool.h"
#include <QAction>

class OSnapTool : public DesignTool
{
    Q_OBJECT
public:
    OSnapTool(DrawingView* view) :
        DesignTool(view)
    {
        _action = new QAction(tr("Object Snap"), _mainWindow);
        _action->setWhatsThis(tr("Enables or disables OSNAP, helping you to pick points on objects"));
        _action->setStatusTip(_action->whatsThis());
        _action->setCheckable(true);
        _action->setChecked(true);
        connect(_action, SIGNAL(changed()), this, SLOT(triggered()));
        _mnemonic = tr("OSNAP");
    }
    
signals:
    
public slots:
    void triggered();
    void start();
    
};

#endif // OSNAP_H
