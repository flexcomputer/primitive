#ifndef SCALE_H
#define SCALE_H

#include <QAction>
#include "designtool.h"

const int STAGE_SELECT_OBJECTS = 0x0;
const int STAGE_PICK_BASE_POINT = 0x1;
const int STAGE_PICK_SCALE_START=0x3;
const int STAGE_SELECT_SCALING_FACTOR = 0x2;
const int STAGE_SELECT_SCALE_X = 0x3;
const int STAGE_SELECT_SCALE_Y = 0x4;

const int MODE_SMOOTH_SCALE = 0x0;
const int MODE_CUSTOM_SCALE = 0x1;

// const int STAGE_SELECT_SCALE_Z = 0x5; // No 3D yet

class ScaleTool : public DesignTool
{
    Q_OBJECT
public:
    ScaleTool(DrawingView* view) :
    DesignTool(view)
    {
        // Create the action
        _action = new QAction(tr("&Scale"), _mainWindow );
        _action->setStatusTip(tr("Scales one or more objects"));
        _action->setIcon(QIcon(":/Icons/Scale.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("SCALE");
    }
    void start();
    void triggered();

    void paintOverlay(QPainter *painter);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    bool highlightObjects() { return stage == STAGE_SELECT_OBJECTS; }

signals:

public slots:
    void textInputEntered(QString text);

protected:
    int stage;
    QVector3D basePoint;
    QVector3D scaleStart;
    QVector3D destPoint;

    float xScale;
    float yScale;
    float zScale;

    void moveToPickBasePoint();
    void moveToPickScaleStart();
    void moveToPickScaleFactor();
    void finishCommand();
    
};

#endif // SCALE_H
