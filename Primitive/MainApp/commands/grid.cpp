#include "grid.h"
#include "settings.h"
#include "drawingview.h"

void GridTool::start()
{
    if (settings.showGrid())
        settings.setShowGrid(false);
    else
        settings.setShowGrid(true);
    _action->setChecked(settings.showGrid());
    _drawingView->repaint();
    emit stop();
}

void GridTool::triggered()
{
    // Don't start the command as this would stop any other command
    // _commandWindow->startCommand(this);
    start();
}
