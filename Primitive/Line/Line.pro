#-------------------------------------------------
#
# Project created by QtCreator 2012-11-04T21:51:22
#
#-------------------------------------------------

QT       += opengl

TARGET = line
TEMPLATE = lib

DEFINES += LINE_LIBRARY

SOURCES += line.cpp

HEADERS += line.h\
        Line_global.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE6C7A54A
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = Line.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
