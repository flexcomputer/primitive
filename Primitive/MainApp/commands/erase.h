#ifndef ERASE_H
#define ERASE_H

#include "../designtool.h"
#include <QAction>
#include <QMessageBox>

class EraseTool : public DesignTool
{
    Q_OBJECT
public:
    EraseTool(DrawingView* view) :
        DesignTool(view),
        deletedObjects(false),
        objectsDeleted(0)
    {
        // Create the action
        _action = new QAction(tr("Erase"), _mainWindow );
        _action->setShortcut(QKeySequence::Delete);
        _action->setStatusTip(tr("Erases one or more objects"));
        _action->setIcon(QIcon(":/Icons/Erase.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("ERASE");
    }

    void start();
    void triggered();

    void paintOverlay(QPainter *painter);
    void keyReleaseEvent(QKeyEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    bool highlightObjects() { return true; }

signals:
    
public slots:
    void textInputEntered(QString text);

protected:
    bool deletedObjects;
    int objectsDeleted;
    
};

#endif // ERASE_H
