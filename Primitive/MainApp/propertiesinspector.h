#ifndef PROPERTIESINSPECTOR_H
#define PROPERTIESINSPECTOR_H

#include <QObject>
#include <QWidget>
#include <QAction>
#include <QDockWidget>

#include "propertymodel.h"
#include "drawingobject.h"

class InspectorWidget : public QWidget
{
    Q_OBJECT
public:
    explicit InspectorWidget(QWidget* parent = 0);

    void addWidget(QString label, QWidget* widget, PropertyType type);
    void clearProperties();
    void createEssentialProperties();
    void setSelectionDescription(QString text);

    QToolBox* toolBox() const { return _toolBox; }

protected:
    QLabel* _selectionDescription;
    QLabel* _generalPropsLabel;
    QLabel* _customPropsLabel;

    QToolBox* _toolBox;

signals:

public slots:

};

class ListProperty;
class CheckProperty;

class PropertiesInspector : public QObject
{
    Q_OBJECT
public:
    explicit PropertiesInspector(ObjectList list, QWidget *parent = 0);
    
    QDockWidget* dockWidget() const { return _dockWidget; }
    InspectorWidget* propsWidget() const { return _widget; }

    // Defaults
    QColor defaultColor() const { return _defaultColor; }
    LineStyle defaultLineStyle() const { return _defaultLineStyle; }
    int defaultLineWidth() const { return _defaultLineWidth; }

protected:
    QDockWidget* _dockWidget;
    InspectorWidget* _widget;

    void createProperties();
    void createEssentialProperties();

    // Essential Widgets
    // Line Color combo box
    ListProperty* lineColorList;
    // Line Style combo box
    ListProperty* lineStyleList;
    // Line Width combo box
    ListProperty* lineWidthList;

    QString colorText(QColor color);
    QString lineStyleText(LineStyle lineStyle);
    QString lineWidthText(int width);

    QColor _defaultColor;
    LineStyle _defaultLineStyle;
    int _defaultLineWidth;


    ObjectList objectList;
signals:
    
public slots:
    void selectionChanged();
    void colorChanged(QString newColor);
    void lineWidthChanged(int newWidth);
    void lineStyleChanged(QString newLineStyle);
};

#endif // PROPERTIESINSPECTOR_H
