#ifndef STATUSBAR_H
#define STATUSBAR_H

#include <QObject>
#include <QMainWindow>
#include <QLabel>

class StatusBar : public QObject
{
    Q_OBJECT
public:
    explicit StatusBar(QMainWindow* mainWindow, QObject *parent = 0);
    
    QMainWindow* parent() const { return _parent; }
    QString posText() const { return _posLabel->text(); }

    void setParent(QMainWindow* parent) { if (parent) _parent = parent; }
    void setPosText(QString text) { _posLabel->setText(text); }

protected:
    QMainWindow* _parent;
    QLabel* _posLabel;
    
};

#endif // STATUSBAR_H
