#include "drawingobject.h"
#include <qmath.h>
#include <QVector3D>


// Since this is a base class, all functions are implemented in the
// header.

bool insideRect(QPoint p, QRect r)
{
    return ((p.x() > r.left()) && (p.x() < r.right())
            && (p.y() > r.top()) && (p.y() < r.bottom()));
}

QVector3D DrawingObject::transformPoint(float x, float y, float z) const
{
    return transformPoint(QVector3D(x, y, z));
}

QVector3D DrawingObject::transformPoint(QVector3D point) const
{
    QVector3D newPoint(point.x() - _position.x(), point.y() - _position.y(), point.z() - _position.z());

    // Rotate the vertex
    float radianValue = rotation.z()/180*PI;
    float sinTheta = sin(radianValue);
    float cosTheta = cos(radianValue);
    // Unfortunately, we can't store the scaled coordinates as a variable
    // and we have to do it this inefficient way.
    // If you solve this let me know
    float newX = newPoint.x() * scaling.x() * cosTheta - newPoint.y() * scaling.y() * sinTheta;
    float newY = newPoint.x() * scaling.x() * sinTheta + newPoint.y() * scaling.y() * cosTheta;
    float newZ = newPoint.z() * scaling.z();

    // Translate the vertex back
    newPoint.setX(newX + _position.x());
    newPoint.setY(newY + _position.y());
    newPoint.setZ(newZ + _position.z());
    // Return the vertex
    return newPoint;
}

QVector3D DrawingObject::detransformPoint(float x, float y, float z) const
{
    return detransformPoint(x, y, z);
}

QVector3D DrawingObject::detransformPoint(QVector3D point) const
{
    QVector3D newPoint(point.x() - _position.x(), point.y() - _position.y(), point.z() - _position.z());

    // Rotate the vertex
    float radianValue = (-rotation.z())/180*PI;
    float sinTheta = sin(radianValue);
    float cosTheta = cos(radianValue);
    // Unfortunately, we can't store the scaled coordinates as a variable
    // and we have to do it this inefficient way.
    // If you solve this let me know
    float scalingX = 1/scaling.x();
    float scalingY = 1/scaling.y();
    float scalingZ = 1/scaling.z();
    float newX = newPoint.x() * cosTheta - newPoint.y() * sinTheta;
    float newY = newPoint.x() * sinTheta + newPoint.y() * cosTheta;
    float newZ = newPoint.z();

    // Translate the vertex back
    newPoint.setX(newX * scalingX + _position.x());
    newPoint.setY(newY * scalingY + _position.y());
    newPoint.setZ(newZ * scalingZ + _position.z());
    // Return the vertex
    return newPoint;
}
