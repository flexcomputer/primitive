/****************************************************************************
** Meta object code from reading C++ file 'propertiestoolbar.h'
**
** Created: Sat Dec 8 11:23:38 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/propertiestoolbar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'propertiestoolbar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PropertiesToolBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x0a,
      47,   38,   18,   18, 0x0a,
      78,   69,   18,   18, 0x0a,
     113,  100,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_PropertiesToolBar[] = {
    "PropertiesToolBar\0\0selectionChanged()\0"
    "newColor\0colorChanged(QString)\0newWidth\0"
    "lineWidthChanged(int)\0newLineStyle\0"
    "lineStyleChanged(QString)\0"
};

void PropertiesToolBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PropertiesToolBar *_t = static_cast<PropertiesToolBar *>(_o);
        switch (_id) {
        case 0: _t->selectionChanged(); break;
        case 1: _t->colorChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->lineWidthChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->lineStyleChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PropertiesToolBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PropertiesToolBar::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PropertiesToolBar,
      qt_meta_data_PropertiesToolBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PropertiesToolBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PropertiesToolBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PropertiesToolBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PropertiesToolBar))
        return static_cast<void*>(const_cast< PropertiesToolBar*>(this));
    return QObject::qt_metacast(_clname);
}

int PropertiesToolBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
