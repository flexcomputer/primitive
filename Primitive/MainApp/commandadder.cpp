#include "drawingview.h"
#include <QMenuBar>
#include <QToolBar>

// Command headers
#include "commands.h"

void DrawingView::addCommands()
{
    // Create the commands

    eraseTool = new EraseTool(this);
    mainDesignTool->addCommand(eraseTool);

    moveTool = new MoveTool(this);
    mainDesignTool->addCommand(moveTool);

    scaleTool = new ScaleTool(this);
    mainDesignTool->addCommand(scaleTool);

    rotateTool = new RotateTool(this);
    mainDesignTool->addCommand(rotateTool);

    newTool = new NewTool(this);
    mainDesignTool->addCommand(newTool);

    saveAsTool = new SaveAsTool(this);
    mainDesignTool->addCommand(saveAsTool);

    saveTool = new SaveTool(this);
    mainDesignTool->addCommand(saveTool);

    openTool = new OpenTool(this);
    mainDesignTool->addCommand(openTool);

    dwgPropsTool = new DwgPropsTool(this);
    mainDesignTool->addCommand(dwgPropsTool);

    quitTool = new QuitTool(this);
    mainDesignTool->addCommand(quitTool);

    aboutTool = new AboutTool(this);
    mainDesignTool->addCommand(aboutTool);

    lineTool = new LineTool(this);
    mainDesignTool->addCommand(lineTool);

    rectTool = new RectTool(this);
    mainDesignTool->addCommand(rectTool);

    gridTool = new GridTool(this);
    mainDesignTool->addCommand(gridTool);

    orthoTool = new OrthoTool(this);
    mainDesignTool->addCommand(orthoTool);

    osnapTool = new OSnapTool(this);
    mainDesignTool->addCommand(osnapTool);

    snapToGridTool = new SnapToGridTool(this);
    mainDesignTool->addCommand(snapToGridTool);

    dynamicInfoTool = new DynamicInfoTool(this);
    mainDesignTool->addCommand(dynamicInfoTool);

    undoTool = new UndoTool(this);
    mainDesignTool->addCommand(undoTool);

    selectAllTool = new SelectAllTool(this);
    mainDesignTool->addCommand(selectAllTool);

    csTool = new CSTool(this);
    mainDesignTool->addCommand(csTool);

    // Create the menu bar
    QMenuBar* menuBar = mainWin->menuBar();

    // Add the menus
    QMenu* fileMenu = menuBar->addMenu(tr("&File"));
    fileMenu->addAction(newTool->action());
    fileMenu->addAction(openTool->action());
    fileMenu->addSeparator();
    fileMenu->addAction(saveTool->action());
    fileMenu->addAction(saveAsTool->action());
    fileMenu->addSeparator();
    fileMenu->addAction(dwgPropsTool->action());
    fileMenu->addSeparator();
    fileMenu->addAction(quitTool->action());
    QMenu* editMenu = menuBar->addMenu(tr("&Edit"));
    editMenu->addAction(undoTool->action());
    editMenu->addSeparator();
    editMenu->addAction(selectAllTool->action());
    editMenu->addAction(csTool->action());
    QMenu* viewMenu = menuBar->addMenu(tr("&View"));
    viewMenu->addAction(gridTool->action());
    QMenu* drawMenu = menuBar->addMenu(tr("&Draw"));
    drawMenu->addAction(lineTool->action());
    drawMenu->addAction(rectTool->action());
    QMenu* modifyMenu = menuBar->addMenu(tr("&Modify"));
    modifyMenu->addAction(eraseTool->action());
    modifyMenu->addSeparator();
    modifyMenu->addAction(moveTool->action());
    modifyMenu->addAction(rotateTool->action());
    modifyMenu->addAction(scaleTool->action());
    QMenu* helpMenu = menuBar->addMenu(tr("&Help"));
    whatsThisAct = new QAction(tr("What's This?"), this);
    whatsThisAct->setShortcut(QKeySequence::WhatsThis);
    helpMenu->addAction(whatsThisAct);
    helpMenu->addAction(aboutTool->action());
}

void DrawingView::createToolbars()
{
    QSize sideToolBarIconSize(32, 32);
    QSize topToolBarIconSize(32, 32);
    QToolBar* drawToolbar = new QToolBar(tr("Draw"), mainWin);
    drawToolbar->addAction(lineTool->action());
    drawToolbar->addAction(rectTool->action());
    drawToolbar->setIconSize(sideToolBarIconSize);
    mainWin->addToolBar(Qt::LeftToolBarArea, drawToolbar);

    QToolBar* modifyToolbar = new QToolBar(tr("Modify"), mainWin);
    modifyToolbar->addAction(eraseTool->action());
    modifyToolbar->addSeparator();
    modifyToolbar->addAction(moveTool->action());
    modifyToolbar->addAction(rotateTool->action());
    modifyToolbar->addAction(scaleTool->action());
    modifyToolbar->setIconSize(sideToolBarIconSize);
    mainWin->addToolBar(Qt::LeftToolBarArea, modifyToolbar);

    QToolBar* fileToolbar = new QToolBar(tr("File"), mainWin);
    fileToolbar->addAction(newTool->action());
    fileToolbar->addAction(openTool->action());
    fileToolbar->addSeparator();
    fileToolbar->addAction(saveTool->action());
    fileToolbar->addAction(dwgPropsTool->action());
    fileToolbar->setIconSize(topToolBarIconSize);
    mainWin->addToolBar(Qt::TopToolBarArea, fileToolbar);

    QToolBar* editToolbar = new QToolBar(tr("Edit"), mainWin);
    editToolbar->addAction(undoTool->action());
    editToolbar->setIconSize(topToolBarIconSize);
    mainWin->addToolBar(Qt::TopToolBarArea, editToolbar);

    QToolBar* drawingToolbar = new QToolBar(tr("Drawing"), mainWin);
    drawingToolbar->addAction(snapToGridTool->action());
    drawingToolbar->addAction(gridTool->action());
    drawingToolbar->addAction(orthoTool->action());
    drawingToolbar->addAction(osnapTool->action());
    drawingToolbar->addAction(dynamicInfoTool->action());
    mainWin->addToolBar(Qt::BottomToolBarArea, drawingToolbar);

    // mainWin->addToolBar(Qt::TopToolBarArea, propertiesToolBar->toolBar());
}

// EOF
