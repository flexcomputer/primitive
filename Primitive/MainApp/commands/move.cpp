#include "move.h"
#include "drawingview.h"

void MoveTool::start()
{
    // Check if there is one object selected
    stage = SELECT_OBJECTS;
    displacement = false;

    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            stage = PICK_BASE_POINT;
        }
    }

    // Set the command window to the correct prompt
    _commandWindow->setCommandName(tr("Move"));
    singleObjectSelection = false;
    _crosshair->setState(Crosshair::SelectObject);
    _crosshair->setPrompt(tr("Select objects to move"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Select objects to move or [Cancel]"));
    if (stage == PICK_BASE_POINT)
        moveToPickBasePoint();
}

void MoveTool::triggered()
{
    _commandWindow->startCommand(this);
}

void MoveTool::paintOverlay(QPainter *painter)
{
    if (stage == SELECT_OBJECTS)
        selectPaintOverlay(painter);
    else
        pickPointPaintOverlay(painter);
}

void MoveTool::keyReleaseEvent(QKeyEvent *e)
{
    if (stage == SELECT_OBJECTS)
    {
        selectKeyReleaseEvent(e);
        if (e->key() == Qt::Key_Return)
        {
            // Move to point selection mode
            moveToPickBasePoint();
        }
    }
    // Check for the Escape
    if ((e->key() == Qt::Key_Escape) ||
            (e->key() == Qt::Key_C))
    {
        Q_FOREACH(DrawingObject* object, *_objects)
        {
            if (object->selected())
            {
                // Remove the selection
                object->setSelected(false);
                object->move(
                            destPoint.x(), destPoint.y(), destPoint.z(),
                            basePoint.x(), basePoint.y(), basePoint.z()
                            );
            }
        }
        emit stop();
        _crosshair->setShowPrompt(false);
        return;
    }
}

void MoveTool::mouseReleaseEvent(QMouseEvent *e)
{
    if (stage == SELECT_OBJECTS)
    {
        selectMouseReleaseEvent(e);
        _crosshair->setState(Crosshair::SelectObject);
    }
    else
    {
        pickPointMouseReleaseEvent(e);
        // The user picked a point, proceed to the next step
        if (stage == PICK_BASE_POINT)
        {
            basePoint = point;
            moveToPickDestPoint();
        }
        else if (stage == PICK_DEST_POINT)
        {
            destPoint = point;
            finishCommand();
        }
    }
}

void MoveTool::mousePressEvent(QMouseEvent *e)
{
    if (stage == SELECT_OBJECTS)
        selectMousePressEvent(e);
        ;
}

void MoveTool::mouseMoveEvent(QMouseEvent *e)
{
    if (stage == SELECT_OBJECTS)
        ;
    else
    {
        pickPointMouseMoveEvent(e);
        if (stage == PICK_DEST_POINT)
        {
            float x, y, z;
            _view->camera()->logicalPoint2D(
                        QPoint(_crosshair->x(), _crosshair->y()), x, y, z);
            QVector3D tempDestPoint = pointToPick;
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                if (object->selected())
                {
                    object->move(
                                destPoint.x(), destPoint.y(), destPoint.z(),
                                tempDestPoint.x(), tempDestPoint.y(), tempDestPoint.z());
                }
            }
            destPoint = tempDestPoint;
        }
    }
}

void MoveTool::textInputEntered(QString text)
{
    // Check if the input is "Cancel"
    if ((text.toUpper() == tr("CANCEL")) ||
         (text.toUpper() == tr("C")))
    {
        // Terminate the command
        emit stop();
        _crosshair->setShowPrompt(false);
        return;
    }
    if (stage == PICK_BASE_POINT)
    {
        // Check if the user inserted a point
        if (pickPointInputEntered(text))
        {
            basePoint = point;
            moveToPickDestPoint();
        }
        else
        {
            if (text.toUpper() == tr("Displacement") || text.toUpper() == tr("D"))
            {
                basePoint.setX(0);
                basePoint.setY(0);
                basePoint.setZ(0);
                displacement = true;
                moveToPickDestPoint();
            }
        }
    }
    else if (stage == PICK_DEST_POINT)
    {
        // Check if the user inserted a point
        if (pickPointInputEntered(text))
        {
            Q_FOREACH(DrawingObject* object, *_objects)
            {
                if (object->selected())
                {
                    object->move(
                                destPoint.x(), destPoint.y(), destPoint.z(),
                                point.x(), point.y(), point.z());
                }
            }
            finishCommand();
        }

    }
}

void MoveTool::moveToPickBasePoint()
{
    // Check if there are objects selected
    bool selected = false;
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected()) selected = true;
    }

    if (!selected)
    {
        // Quit and inform the user
        _crosshair->setPrompt(tr("No objects were selected"));
        emit stop();
        return;
    }

    stage = PICK_BASE_POINT;
    _crosshair->setState(Crosshair::PickPoint);
    pickPointSetPrompt(tr("Specify base point"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Specify base point or [Displacement/Cancel]"));
}

void MoveTool::moveToPickDestPoint()
{
    stage = PICK_DEST_POINT;
    destPoint = basePoint;
    lastPoint = basePoint;
    pickPointSetPrompt(tr("Specify destination point"));
    _crosshair->setShowPrompt(true);
    _commandWindow->setPrompt(tr("Specify destination point or [Cancel]"));
    if (displacement)
    {
        _crosshair->setPrompt(tr("Insert displacement"));
        _commandWindow->setPrompt(tr("Insert displacement <X,Y,Z> or [Cancel]"));
    }
    // Set the previous point
    this->lastPoint = basePoint;
}

void MoveTool::finishCommand()
{
    int objectCount = 0;
    Q_FOREACH(DrawingObject* object, *_objects)
    {
        if (object->selected())
        {
            object->setSelected(false);
            objectCount++;
        }
    }
    _crosshair->setShowPrompt(false);
    _drawingView->documentModified(tr("Moved %1 object(s)", "", objectCount).arg(objectCount));
    emit stop();
    // Mission accomplished
}
