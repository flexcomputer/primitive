/****************************************************************************
** Meta object code from reading C++ file 'dwgrectangle.h'
**
** Created: Sun Dec 9 18:08:12 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/objects/dwgrectangle.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dwgrectangle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_DwgRectangle[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_DwgRectangle[] = {
    "DwgRectangle\0\0rotate90()\0"
};

void DwgRectangle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        DwgRectangle *_t = static_cast<DwgRectangle *>(_o);
        switch (_id) {
        case 0: _t->rotate90(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DwgRectangle::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DwgRectangle::staticMetaObject = {
    { &DrawingObject::staticMetaObject, qt_meta_stringdata_DwgRectangle,
      qt_meta_data_DwgRectangle, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DwgRectangle::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DwgRectangle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DwgRectangle::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DwgRectangle))
        return static_cast<void*>(const_cast< DwgRectangle*>(this));
    return DrawingObject::qt_metacast(_clname);
}

int DwgRectangle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DrawingObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
