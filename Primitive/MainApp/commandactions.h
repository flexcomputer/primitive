#ifndef COMMANDACTIONS_H
#define COMMANDACTIONS_H

protected:
EraseTool* eraseTool;
MoveTool* moveTool;
ScaleTool* scaleTool;
RotateTool* rotateTool;
SaveAsTool* saveAsTool;
SaveTool* saveTool;
OpenTool* openTool;
AboutTool* aboutTool;
NewTool* newTool;
DwgPropsTool* dwgPropsTool;
QuitTool* quitTool;
LineTool* lineTool;
RectTool* rectTool;
GridTool* gridTool;
OrthoTool* orthoTool;
SnapToGridTool* snapToGridTool;
DynamicInfoTool* dynamicInfoTool;
OSnapTool* osnapTool;
UndoTool* undoTool;
SelectAllTool* selectAllTool;
CSTool* csTool;

#endif // COMMANDACTIONS_H
