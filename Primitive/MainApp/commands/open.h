#ifndef OPENTOOL_H
#define OPENTOOL_H

#include "../designtool.h"
#include <QAction>
#include <QFileDialog>
#include <QObject>

class OpenTool : public DesignTool
{
    Q_OBJECT
public:
    OpenTool(DrawingView* view) :
        DesignTool(view)
    {
        // Create the actions
        _action = new QAction(
                    tr("&Open..."), _mainWindow);
        _action->setStatusTip(tr("Opens an existing drawing"));
        _action->setShortcut(QKeySequence::Open);
        _action->setIcon(QIcon(":/Icons/Open.png"));
        connect(_action, SIGNAL(triggered()), this, SLOT(triggered()));
        // Set the mnemonic
        _mnemonic = tr("OPEN");
        // Add additional construction code here
    }
signals:
    
public slots:
    void start();
    void triggered();
};

#endif // OPENTOOL_H
