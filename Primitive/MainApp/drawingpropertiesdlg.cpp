#include "drawingpropertiesdlg.h"
#include <QHBoxLayout>
#include <QVBoxLayout>

DrawingPropertiesDlg::DrawingPropertiesDlg(QWidget *parent, QString drawingName,
                                           QString authorName,
                                           int drawingUnit) :
    QDialog(parent)
{
    _drawingName = drawingName;
    nameLabel = new QLabel(tr("Drawing Title"), this);
    drawingNameEdit = new QLineEdit(drawingName, this);
    drawingNameEdit->setEnabled(true);
    drawingNameEdit->setWhatsThis(tr("The title of the drawing.\n"
                                     "E.g. \"First Floor\", \"East Dock\""));
    nameLabel->setBuddy(drawingNameEdit);
    authorLabel = new QLabel(tr("Drawing Author(s) (names seperated by \";\")"), this);
    authorEdit = new QLineEdit(_authorName, this);
    authorEdit->setWhatsThis(tr("The name of the author of the drawing.\n"));
    authorLabel->setBuddy(authorEdit);
    unitLabel = new QLabel(tr("Distance Unit"), this);
    unitBox = new QComboBox(this);
    unitBox->setWhatsThis(tr("Specifies the distance unit for this drawing.\n"
                             "The distance unit affects the format of dimensions within"
                             " the drawing."));
    unitLabel->setBuddy(unitBox);
    saveButton = new QPushButton(tr("Save"), this);
    saveButton->setEnabled(true);
    saveButton->setWhatsThis(tr("Saves your changes and hides this dialog."));
    defaultsButton = new QPushButton(tr("Defaults"), this);
    defaultsButton->setEnabled(true);
    defaultsButton->
            setWhatsThis(tr("Loads the default values without hiding this dialog."));
    cancelButton = new QPushButton(tr("Cancel"), this);
    cancelButton->setEnabled(true);
    cancelButton->setWhatsThis(tr("Hides this dialog without saving your changes."));
    connect(saveButton, SIGNAL(clicked()), SLOT(saveClicked()));
    connect(defaultsButton, SIGNAL(clicked()), SLOT(defaultsClicked()));
    connect(cancelButton, SIGNAL(clicked()), SLOT(cancelClicked()));

    // Create the layouts
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    QHBoxLayout* topLayout = new QHBoxLayout;
    QHBoxLayout* bottomLayout = new QHBoxLayout;
    QHBoxLayout* unitLayout = new QHBoxLayout;
    QHBoxLayout* authorNameLayout = new QHBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(authorNameLayout);
    mainLayout->addLayout(unitLayout);
    mainLayout->addLayout(bottomLayout);

    topLayout->addWidget(nameLabel);
    topLayout->addWidget(drawingNameEdit);
    bottomLayout->addWidget(defaultsButton);
    bottomLayout->addWidget(cancelButton);
    bottomLayout->addWidget(saveButton);

    authorNameLayout->addWidget(authorLabel);
    authorNameLayout->addWidget(authorEdit);

    unitLayout->addWidget(unitLabel);
    unitLayout->addWidget(unitBox);

    // Add the units to the unit box
    unitBox->addItem(tr("Foot"));
    unitBox->addItem(tr("Inch"));
    unitBox->addItem(tr("Meter"));
    unitBox->addItem(tr("Decimeter"));
    unitBox->addItem(tr("Centimeter"));
    unitBox->addItem(tr("Millimeter"));
    unitBox->addItem(tr("Micrometer"));
    unitBox->addItem(tr("Nanometer"));
    unitBox->addItem(tr("Picometer"));
    unitBox->addItem(tr("Femtometer"));
    unitBox->addItem(tr("Attometer"));
    unitBox->addItem(tr("Zeptometer"));
    unitBox->addItem(tr("Yoctometer"));
    unitBox->addItem(tr("Dekameter"));
    unitBox->addItem(tr("Hectometer"));
    unitBox->addItem(tr("Kilometer"));
    unitBox->addItem(tr("Megameter"));
    unitBox->addItem(tr("Gigameter"));
    unitBox->addItem(tr("Terameter"));
    unitBox->addItem(tr("Petameter"));
    unitBox->addItem(tr("Exameter"));
    unitBox->addItem(tr("Zettameter"));
    unitBox->addItem(tr("Yottameter"));
    unitBox->addItem(tr("Mile"));
    unitBox->addItem(tr("Sea Mile"));
    unitBox->addItem(tr("Lightyear"));

    unitBox->setCurrentIndex(drawingUnit);
    setDistanceUnit((DistanceUnit)drawingUnit);

    authorEdit->setText(authorName);
    setAuthorName(authorName);
    setLayout(mainLayout);
    resize(width() + 300, height());
}

void DrawingPropertiesDlg::saveClicked()
{
    // Set the drawing title
    _drawingName = drawingNameEdit->text();
    _authorName = authorEdit->text();
    _unit = (DistanceUnit)unitBox->currentIndex();
    this->close();
}

void DrawingPropertiesDlg::defaultsClicked()
{
    // Restore the default values
    _drawingName = tr("Drawing");
    drawingNameEdit->setText(tr("Drawing"));
    drawingNameEdit->selectAll();
    drawingNameEdit->setFocus();
    _unit = settings.defaultDistanceUnit();
    unitBox->setCurrentIndex((int)_unit);
    _authorName = tr("");
    authorEdit->setText(authorName());
}

void DrawingPropertiesDlg::cancelClicked()
{
    // Close the window returning "Cancel"
    this->close();
}
