#include "plugincontroller.h"

PluginController::PluginController(DrawingView* view) :
    DesignTool(view)
{
}

void PluginController::loadPlugin(DesignToolPlugin *plugin)
{
    plugin->setDrawingView(_drawingView);
    _plugin = plugin;
    _action = plugin->action();
    _mnemonic = plugin->mnemonic();
}

void PluginController::mousePressEvent(QMouseEvent *e)
{
    _plugin->mousePressEvent(e);
}

void PluginController::mouseReleaseEvent(QMouseEvent *e)
{
    _plugin->mouseReleaseEvent(e);
}

void PluginController::mouseMoveEvent(QMouseEvent *e)
{
    _plugin->mouseMoveEvent(e);
}

void PluginController::keyPressEvent(QKeyEvent *e)
{
    _plugin->keyPressEvent(e);
}

void PluginController::keyReleaseEvent(QKeyEvent *e)
{
    _plugin->keyReleaseEvent(e);
}

void PluginController::paintGL()
{
    _plugin->paintGL(_view);
}

void PluginController::paintOverlay(QPainter *painter)
{
    _plugin->paintOverlay(painter);
}

bool PluginController::highlightObjects()
{
    return _plugin->highlightObjects();
}

QString PluginController::mnemonic() const
{
    return _plugin->mnemonic();
}

QAction* PluginController::action() const
{
    return _plugin->action();
}

void PluginController::textInputEntered(QString text)
{
    _plugin->textInputEntered(text);
}

void PluginController::start()
{
    _plugin->start();
}

void PluginController::triggered()
{
    if (_plugin->noInteraction())
        _plugin->start();
    else
        _commandWindow->startCommand(this);
}

void PluginController::createContextMenu(QMenu *menu)
{
    _plugin->createContextMenu(menu);
}
