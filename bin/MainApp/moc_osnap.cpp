/****************************************************************************
** Meta object code from reading C++ file 'osnap.h'
**
** Created: Sat Dec 8 11:24:51 2012
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Primitive/MainApp/commands/osnap.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'osnap.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_OSnapTool[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x0a,
      23,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_OSnapTool[] = {
    "OSnapTool\0\0triggered()\0start()\0"
};

void OSnapTool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        OSnapTool *_t = static_cast<OSnapTool *>(_o);
        switch (_id) {
        case 0: _t->triggered(); break;
        case 1: _t->start(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData OSnapTool::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject OSnapTool::staticMetaObject = {
    { &DesignTool::staticMetaObject, qt_meta_stringdata_OSnapTool,
      qt_meta_data_OSnapTool, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &OSnapTool::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *OSnapTool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *OSnapTool::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_OSnapTool))
        return static_cast<void*>(const_cast< OSnapTool*>(this));
    return DesignTool::qt_metacast(_clname);
}

int OSnapTool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = DesignTool::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
